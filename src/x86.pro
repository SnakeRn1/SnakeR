QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
CONFIG += c++11
DEFINES += QT_DEPRECATED_WARNINGS
SOURCES += main.cpp snaker.cpp
HEADERS += snaker.h
FORMS += snaker.ui
VERSION = 1.0.4
RESOURCES += res.qrc

win32 {
INCLUDEPATH += C:/OpenCV/include
LIBS += -LC:\OpenCV\x86
LIBS += -lopencv_core451 -lopencv_imgproc451
TARGET = ..\..\SnakeR_x86\SnakeR_x86
RC_ICONS += ico.ico
QMAKE_TARGET_COMPANY = "SnakeR Soft"
QMAKE_TARGET_PRODUCT = "SnakeR_x86"
QMAKE_TARGET_DESCRIPTION = "SnakeR_x86"
QMAKE_TARGET_COPYRIGHT = "Copyright © 2021 Horuzhiy Ruslan"
}

unix {
INCLUDEPATH += /usr/local/include/opencv4 /usr/include/opencv4
LIBS += -lopencv_core -lopencv_imgproc
TARGET = ../linux-bin/SnakeR_x86
QMAKE_LFLAGS += -no-pie
RC_ICONS += ico.ico
QMAKE_TARGET_COMPANY = "SnakeR Soft"
QMAKE_TARGET_PRODUCT = "SnakeR_x86"
QMAKE_TARGET_DESCRIPTION = "SnakeR_x86"
QMAKE_TARGET_COPYRIGHT = "Copyright © 2021 Horuzhiy Ruslan"
}

qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
