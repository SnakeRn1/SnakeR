#ifndef SNAKER_H
#define SNAKER_H

#include <QMainWindow>
#include <QImage>
#include <QPixmap>
#include <QTimer>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QFocusEvent>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <ctime>
#include <vector>

enum moving { UP, DOWN, LEFT, RIGHT };

QT_BEGIN_NAMESPACE
namespace Ui { class SnakeR; }
QT_END_NAMESPACE

class SnakeR : public QMainWindow
{
    Q_OBJECT

public:
    SnakeR(QWidget *parent = nullptr);
    ~SnakeR();

private slots:
    void tim_func();

    void on_speed_stateChanged(int arg1);

private:
    Ui::SnakeR *ui;

    int chUs();
    int chDs();
    int chLs();
    int chRs();
    int chUd();
    int chDd();
    int chLd();
    int chRd();

    bool isH();
    bool isV();

    void printFood(const cv::Point& poi);
    void printHead(const cv::Point& poi);
    void printBody(const cv::Point& poi);
    void deleteObject(const cv::Point &poi);
    void et_print(cv::Mat &img, int lineOffsY, const std::string &ss);
    void init_board(bool computer = 0, int board_width = 16, int board_height = 9, int interpolation = 40);


    int wi,he,interp;

    moving move;
    bool game_process;
    bool pause_state;

    cv::Point head,food;

    std::vector<cv::Point> arch;

    time_t tim;
    cv::Mat img_food,img_head,img_body,img_over,img_back,board_to_show_all,board_to_show,text_win;

    cv::Size board_size,sq_size,full_size;
    cv::Rect game_rect,text_rect;

    int score;
    bool cpu;

    QImage qt_img;
    QPixmap pixmap;
    QTimer* timer;

    std::vector<cv::Mat> win_win;

    void pause();
    void generate_background(cv::Mat&, int, uint, uint, uint);
protected:
    void keyPressEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void focusOutEvent(QFocusEvent *event);
};
#endif // SNAKER_H
