#include "snaker.h"
#include "ui_snaker.h"

using namespace cv;
using namespace std;

SnakeR::SnakeR(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::SnakeR)
{
    ui->setupUi(this);
    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(tim_func()));
    init_board(true);
}

SnakeR::~SnakeR()
{
    delete ui;
}

void SnakeR::init_board(bool computer, int board_width, int board_height, int interpolation)
{
    pause_state = false;
    timer->stop();
    wi = board_width;
    he = board_height;
    interp = interpolation;
    cpu = computer;
    int timer_ms = 500;
    if(ui->speed->isChecked()) timer_ms = 300;
    if(cpu) timer_ms = 50;

    board_size  = Size (wi,he);
    sq_size     = Size  (interp,interp);
    full_size   = board_size*interp;
    game_rect   = Rect (Point(0,0),full_size);
    text_rect   = Rect (0,full_size.height,full_size.width,50);

    QFile file(":/data");
    file.open(QIODevice::ReadOnly);
    cv::FileStorage storeRD;
    storeRD.open(file.readAll().data(),FileStorage::MEMORY);
    file.close();
    if(storeRD.isOpened()&&ui->jewish->isChecked())
    {
        storeRD["img_food"]>>img_food;
        storeRD["img_head"]>>img_head;
        storeRD["img_body"]>>img_body;
        storeRD["img_over"]>>img_over;
        storeRD["img_back"]>>img_back;
        storeRD.release();
/*
        cv::FileStorage wr ("new",FileStorage::WRITE);
        for (int i(1);i<9;++i)
        {
            cv::String addr("imgs/"+to_string(i)+".jpg");
            cout<<addr<<endl;
            Mat iuda = imread(addr,IMREAD_GRAYSCALE);
            cv::resize(iuda,iuda,Size(320,180));
            wr<<("win_"+to_string(i))<<iuda;
        }
        //wr<<"img_food"<<img_food<<"img_head"<<img_head<<"img_body"<<img_body<<"img_over"<<img_over<<"img_back"<<img_back;
        wr.release();
*/
    }
    else
    {
        img_food.release();
        img_head.release();
        img_body.release();
        img_over.release();
        img_back.release();
    }

    QFile wins_file(":/win_imgs");
    wins_file.open(QIODevice::ReadOnly);
    cv::FileStorage wins_store;
    wins_store.open(wins_file.readAll().data(),FileStorage::MEMORY);
    wins_file.close();
    if(wins_store.isOpened())
    {
        for (int i(1);i<9;++i)
        {
            Mat WinWinImg;
            wins_store["win_"+to_string(i)]>>WinWinImg;
            if(!WinWinImg.empty())
            {
                cvtColor(WinWinImg,WinWinImg,COLOR_GRAY2BGRA);
                win_win.push_back(WinWinImg);
            }
        }
        wins_store.release();
    }

    if(!img_food.empty()) cv::resize(img_food,img_food,sq_size,0,0,INTER_AREA);
    if(!img_head.empty()) cv::resize(img_head,img_head,sq_size,0,0,INTER_AREA);
    if(!img_body.empty()) cv::resize(img_body,img_body,sq_size,0,0,INTER_AREA);
    if(img_over.empty()) img_over = Mat(full_size,CV_8UC4,Scalar(0,0,255,255));
    else cv::resize(img_over,img_over,full_size,0,0,INTER_CUBIC);
    if(img_back.empty())
    {
        img_back = Mat(full_size,CV_8UC4,Scalar(0,0,0,255));
        generate_background(img_back,20,0,50,0);
    }
    else cv::resize(img_back,img_back,full_size,0,0,INTER_AREA);

    qt_img = QImage (full_size.width, (full_size.height+50), QImage::Format_RGBA8888);
    board_to_show_all   = cv::Mat(full_size.height+50,full_size.width,CV_8UC4,qt_img.bits());
    board_to_show       = cv::Mat(board_to_show_all,game_rect);
    text_win            = cv::Mat(board_to_show_all,text_rect);
    text_win.setTo(Scalar(0,0,0,255));
    generate_background(text_win,20,0,0,50);

    move = UP;
    score = 0;
    game_process = true;

    head = cv::Point(board_size.width/2,board_size.height/2);
    food = cv::Point(rand()%board_size.width,rand()%board_size.height);

    if(!arch.empty()) arch.clear();

    time(&tim);
    srand (static_cast<uint>(tim));

    img_back.copyTo(board_to_show);

    printFood(food);
    printHead(head);

    et_print(text_win,0,"score: "+to_string(arch.size()));

    pixmap = QPixmap::fromImage(qt_img.rgbSwapped());
    ui->pixmap->setPixmap(pixmap);
    timer->start(timer_ms);
    setFocus();
}

void SnakeR::generate_background(Mat& img, int sq, uint b_max, uint g_max, uint r_max)
{
    if(!img.empty())
    {
        int sq_size = sq;
        int v_sqrs = img.rows/sq_size;
        int h_sqrs = img.cols/sq_size;
        for (int h(0);h<h_sqrs;++h)
        {
            int x = h*sq_size;
            for (int v(0);v<v_sqrs;++v)
            {
                int y = v*sq_size;

                int b(0);
                int g(0);
                int r(0);
                if(b_max>0) b = cv::randu<uint>()%b_max;
                if(g_max>0) g = cv::randu<uint>()%g_max;
                if(r_max>0) r = cv::randu<uint>()%r_max;
                rectangle(img,Rect(x+2,y+2,sq_size-4,sq_size-4),Scalar(b,g,r,255),-1);
                if(b_max>0) b = cv::randu<uint>()%b_max;
                if(g_max>0) g = cv::randu<uint>()%g_max;
                if(r_max>0) r = cv::randu<uint>()%r_max;
                rectangle(img,Rect(x+1,y+1,sq_size-2,sq_size-2),Scalar(b,g,r,255),1,LINE_AA);
            }
        }
    }
}

void SnakeR::tim_func()
{
    if(game_process)
    {
        arch.push_back(head);
        switch (move)
        {
        case(UP):
            if(--head.y < 0) head.y = board_size.height - 1;
            break;
        case(DOWN):
            if(++head.y == board_size.height) head.y = 0;
            break;
        case(LEFT):
            if(--head.x < 0) head.x = board_size.width - 1;
            break;
        case(RIGHT):
            if(++head.x == board_size.width) head.x = 0;
            break;
        }
        if(head == food)
        {
            printHead(food);
            printBody(arch.back());
            bool generating = true;
            int cnt = 0;
            Point prev_food = food;
            while (generating)
            {
                cnt++;
                food.x = rand()%board_size.width;
                food.y = rand()%board_size.height;
                bool repeat = false;
                for(size_t tt=0;tt<arch.size();tt++) if((food==arch.at(tt))||(food == prev_food)) repeat = true;
                if(!repeat) generating = false;
                if (cnt == board_size.area()) game_process = false;
            }
            printFood(food);
            text_win.setTo(Scalar(0,0,0,255));
            generate_background(text_win,20,0,0,60);
            score = static_cast<int>(arch.size());
            if(!cpu) et_print(text_win,0,"score: "+to_string(score));
            else et_print(text_win,0,"CPU PLAYING! score: "+to_string(score));
        }
        else
        {
            deleteObject(arch.front());
            if(!arch.empty()) arch.erase(arch.begin());
            printHead(head);
            if(!arch.empty()) printBody(arch.back());
        }
        for (size_t pp=0;pp<arch.size();pp++) if(head == arch.at(pp)) game_process = false;
        if(score >= 50) game_process = false;
        if(!game_process)
        {
            if(score >= 50)
            {
                cv::resize(win_win.at(rand()%win_win.size()),board_to_show,board_to_show.size());
                timer->stop();
            }
            else
            {
                img_over.copyTo(board_to_show);
                timer->stop();
            }
        }

        pixmap = QPixmap::fromImage(qt_img.rgbSwapped());
        ui->pixmap->setPixmap(pixmap);

        if(cpu)
        {
            moving prestate = move;
            int to_food_h = food.x-head.x;
            int to_food_v = food.y-head.y;
            int to_food_h_abs = abs(to_food_h);
            int to_food_v_abs = abs(to_food_v);

            if((to_food_h>0)&&(isV())) if((chRd()>to_food_h_abs)) move= RIGHT;
            if((to_food_h<0)&&(isV())) if((chLd()>to_food_h_abs)) move = LEFT;
            if(to_food_h == 0)
            {
                if((to_food_v>0)&&(isH())) if((chDd()>to_food_v_abs)) move = DOWN;
                if((to_food_v<0)&&(isH())) if((chUd()>to_food_v_abs)) move = UP;
            }

            if (move==prestate)
            {
                int d_to_move = 1;
                switch(move)
                {
                case(UP):
                    if((chUd()==1))
                    {
                        if(chLs()<chRs())
                        {
                            if(chLd()>d_to_move) move = LEFT;
                            else move = RIGHT;
                        }
                        else
                        {
                            if(chRd()>d_to_move) move = RIGHT;
                            else move = LEFT;
                        }
                    }
                    break;

                case(DOWN):
                    if((chDd()==1))
                    {
                        if(chLs()<chRs())
                        {
                            if(chLd()>d_to_move) move = LEFT;
                            else move = RIGHT;
                        }
                        else
                        {
                            if(chRd()>d_to_move) move = RIGHT;
                            else move = RIGHT;
                        }
                    }
                    break;

                case(LEFT):
                    if((chLd()==1))
                    {
                        if(chUs()<chDs())
                        {
                            if(chUd()>d_to_move) move = UP;
                            else move = DOWN;
                        }
                        else
                        {
                            if(chDd()>d_to_move) move = DOWN;
                            else move = UP;
                        }
                    }
                    break;

                case(RIGHT):
                    if((chRd()==1))
                    {
                        if(chUs()<chDs())
                        {
                            if(chUd()>d_to_move) move = UP;
                            else move = DOWN;
                        }
                        else
                        {
                            if(chDd()>d_to_move) move = DOWN;
                            else move = UP;
                        }
                    }
                    break;
                }
            }
        }
    }

}

void SnakeR::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Space) pause_state = !pause_state;
    if(!pause_state)
    {
        timer->stop();
        if((!game_process)||cpu) {game_process = true; init_board();}
        bool move_block = false;
        switch (event->key())
        {
            case Qt::Key_Up:    if(move != DOWN)    move = UP;      else move_block = true; break;
            case Qt::Key_Down:  if(move != UP)      move = DOWN;    else move_block = true; break;
            case Qt::Key_Left:  if(move != RIGHT)   move = LEFT;    else move_block = true; break;
            case Qt::Key_Right: if(move != LEFT)    move = RIGHT;   else move_block = true; break;
        case Qt::Key_Space:
            pixmap = QPixmap::fromImage(qt_img.rgbSwapped());
            ui->pixmap->setPixmap(pixmap);
            move_block = true;
            break;
        }
        if(!move_block)tim_func();
        timer->start();
    }
    else pause();
}

void SnakeR::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton) if((!game_process)||cpu) {game_process = true; init_board();}
}

void SnakeR::focusOutEvent(QFocusEvent *event)
{
    event->accept();
    pause_state = true;
    pause();
}

void SnakeR::pause()
{
    timer->stop();
    QImage p_q_i (full_size.width, full_size.height+50, QImage::Format_RGBA8888);
    cv::Mat p_c_i (full_size.height+50,full_size.width,CV_8UC4,p_q_i.bits());
    p_c_i.setTo(Scalar(0,50,0,255));
    et_print(p_c_i,0,"PAUSED! Press SPACE! score: "+to_string(score));
    pixmap = QPixmap::fromImage(p_q_i.rgbSwapped());
    ui->pixmap->setPixmap(pixmap);
}

int SnakeR::chUs ()
{
    int detected = 0;
    for (auto ii = end(arch); ii != begin(arch); --ii)
    {
        if((head.x == (*ii).x)&&(head.y>(*ii).y))
            detected = static_cast<int>(distance(arch.begin(),ii));
    }
    return detected;
}

int SnakeR::chDs ()
{
    int detected = 0;
    for (auto ii = end(arch); ii != begin(arch); --ii)
    {
        if((head.x == (*ii).x)&&(head.y<(*ii).y))
            detected = static_cast<int>(distance(arch.begin(),ii));
    }
    return detected;
}

int SnakeR::chLs ()
{
    int detected = 0;
    for (auto ii = end(arch); ii != begin(arch); --ii)
    {
        if((head.y == (*ii).y)&&(head.x>(*ii).x))
            detected = static_cast<int>(distance(arch.begin(),ii));
    }
    return detected;
}

int SnakeR::chRs ()
{
    int detected = 0;
    for (auto ii = end(arch); ii != begin(arch); --ii)
    {
        if((head.y == (*ii).y)&&(head.x<(*ii).x))
            detected = static_cast<int>(distance(arch.begin(),ii));
    }
    return detected;
}

int SnakeR::chUd ()
{
    int detected = 500;
    for (auto ii = begin(arch); ii != end(arch); ++ii)
    {
        if(head.x == (*ii).x)
        {
            int dist = head.y-(*ii).y;
            if(dist<0)
            {
                dist = he+dist;
            }
            if((dist<detected)) detected = dist;
        }
    }
    return detected;
}

int SnakeR::chDd ()
{
    int detected = 500;
    for (auto ii = begin(arch); ii != end(arch); ++ii)
    {
        if(head.x == (*ii).x)
        {
            int dist = (*ii).y-head.y;
            if(dist<0)
            {
                dist = he+dist;
            }
            if((dist<detected)) detected = dist;
        }
    }
    return detected;
}

int SnakeR::chLd ()
{
    int detected = 500;
    for (auto ii = begin(arch); ii != end(arch); ++ii)
    {
        if(head.y == (*ii).y)
        {
            int dist = head.x-(*ii).x;
            if(dist<0)
            {
                dist = wi+dist;
            }
            if((dist<detected)) detected = dist;
        }
    }
    return detected;
}

int SnakeR::chRd ()
{
    int detected = 500;
    for (auto ii = begin(arch); ii != end(arch); ++ii)
    {
        if(head.y == (*ii).y)
        {
            int dist = (*ii).x-head.x;
            if(dist<0)
            {
                dist = wi+dist;
            }
            if((dist<detected)) detected = dist;
        }
    }
    return detected;
}

void SnakeR::et_print(cv::Mat &img, int lineOffsY, const std::string &ss)
{
    int fontFace = cv::FONT_HERSHEY_COMPLEX;
    double fontScale = 1.2;
    int fontThickness = 1;
    cv::Size fontSize = cv::getTextSize("T[]", fontFace, fontScale, fontThickness, nullptr);
    cv::Point org;
    org.x = 1;
    org.y = 3 * fontSize.height * (lineOffsY + 1) / 2;
    putText(img, ss, org, fontFace, fontScale, cv::Scalar(255,100,100), fontThickness, 16);
}

void SnakeR::deleteObject(const Point& poi)
{
    Rect rect (poi.x*interp,poi.y*interp,interp,interp);
    Mat object_to(board_to_show,rect);
    Mat object_from(img_back,rect);
    object_from.copyTo(object_to);
}

void SnakeR::printFood(const Point& poi)
{
    Rect rect (poi.x*interp,poi.y*interp,interp,interp);
    Mat object_to(board_to_show,rect);
    Mat object_from(img_back,rect);
    object_from.copyTo(object_to);
    if(!img_food.empty())object_to += img_food;
    else
    {
        Scalar c (0,200,0,255);
        Scalar s (0,150,0,255);
        Point sqc(interp/2,interp/2);
        Scalar w (200,200,200,255);
        Point w_p = sqc + Point(2,-3);
        Point w_p2 = sqc + Point(10,-12);
        Point w_p3 = sqc + Point(6,-14);
        circle(object_to,sqc,19,s,-1,LINE_AA);
        circle(object_to,w_p,15,c,-1,LINE_AA);
        circle(object_to,w_p2,2,w,-1,LINE_4);
        circle(object_to,w_p3,2,w,-1,LINE_4);
    }
}

void SnakeR::printHead(const Point& poi)
{
    Rect rect (poi.x*interp,poi.y*interp,interp,interp);
    Mat object_to(board_to_show,rect);
    Mat object_from(img_back,rect);
    object_from.copyTo(object_to);
    if(!img_head.empty())object_to += img_head;
    else
    {
        Scalar c (0,0,200,255);
        Scalar s (0,0,150,255);
        Point sqc(interp/2,interp/2);
        Scalar w (200,200,200,255);
        Point w_p = sqc + Point(2,-3);
        Point w_p2 = sqc + Point(10,-12);
        Point w_p3 = sqc + Point(6,-14);
        circle(object_to,sqc,19,s,-1,LINE_AA);
        circle(object_to,w_p,15,c,-1,LINE_AA);
        circle(object_to,w_p2,2,w,-1,LINE_4);
        circle(object_to,w_p3,2,w,-1,LINE_4);
    }
}

void SnakeR::printBody(const Point& poi)
{
    Rect rect (poi.x*interp,poi.y*interp,interp,interp);
    Mat object_to(board_to_show,rect);
    Mat object_from(img_back,rect);
    object_from.copyTo(object_to);
    if(!img_body.empty()) object_to += img_body;
    else
    {
        Scalar c (200,0,0,255);
        Scalar s (150,0,0,255);
        Point sqc(interp/2,interp/2);
        Scalar w (200,200,200,255);
        Point w_p = sqc + Point(2,-3);
        Point w_p2 = sqc + Point(10,-12);
        Point w_p3 = sqc + Point(6,-14);
        circle(object_to,sqc,19,s,-1,LINE_AA);
        circle(object_to,w_p,15,c,-1,LINE_AA);
        circle(object_to,w_p2,2,w,-1,LINE_4);
        circle(object_to,w_p3,2,w,-1,LINE_4);
    }
}

bool SnakeR::isH()
{
    if((move!=UP)&&(move!=DOWN)) return true;
    else return false;
}

bool SnakeR::isV()
{
    if((move!=LEFT)&&(move!=RIGHT)) return true;
    else return false;
}

void SnakeR::on_speed_stateChanged(int arg1)
{
    if(arg1) timer->start(300);
    else timer->start(500);
}
